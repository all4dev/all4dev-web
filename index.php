<?php
session_start();
include("vues/v_header.php");

switch ($_REQUEST['ac']) {

	case 'mentionsLegales': {
		include("vues/v_mentions_legales.php");
		break;
	}

	case 'contact': {
		include("vues/v_contact.php");
		break;
	}

    case 'download': {
        include("vues/v_download.php");
        break;
    }

	default : {
		include("vues/v_inscription.php");
		break;
	}
}

include("vues/v_footer.php");


<div class="signin-form">
    <form class="form-signin" method="post" id="register-form">
        <div id="form-result">
        </div>
        <div class="form-group">
            <div class="col-md-6 col-lg-6 loginColumn">
                <input type="text" class="form-control" placeholder="Nom d'utilisateur" name="username" id="username" />
                <input type="password" class="form-control" placeholder="Mot de passe" name="password" id="password" />
                <input type="text" class="form-control" placeholder="Prénom" name="firstname" id="firstname" />
            </div>
            <div class="col-md-6 col-lg-6 loginColumn">
                <input type="email" class="form-control" placeholder="Adresse e-mail" name="email" id="email" />
                <input type="password" class="form-control" placeholder="Confirmez votre mot de passe" name="cpassword" id="cpassword" />
                <input type="text" class="form-control" placeholder="Nom" name="lastname" id="lastname" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-0 col-md-8"><input  class="btn btn-success btn btn-success" type="submit" value="Inscription"/></div>
        </div>

    </form>

</div>

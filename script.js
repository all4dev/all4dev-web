$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            username: {
                required: true,
                minlength: 3
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            firstname: {
                minlength: 3
            },
            lastname: {
                minlength: 3
            }

        },
        messages:
        {
            username: {
                required: "Entrer un nom d'utilisateur",
                minlength: "Entrer un nom d'utilisateur valide"
            },
            email: "Entrer un e-mail valide",
            password: {
                required: "Entrer un mot de passe",
                minlength: "Le mot de passe doit contenir au moin 8 caractères"
            },

            cpassword: {
                required: "Retaper votre mot de passe",
                equalTo: "Entrer un mot de passe identique !"
            },
            firstname: "Entrer un prenom valide",
            lastname: "Entrer un nom valide"
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        var data = $("#register-form").serialize();

        $.ajax({

            type : 'POST',
            url  : 'http://api.all4dev.xyz/users',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success :  function(data) {
                $("#form-result").fadeIn(200, function() {
                    $("#register-form").trigger('reset')
                    $("#form-result").html('<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> &nbsp; Votre compte a été créer !</div>');
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#form-result").fadeIn(200, function() {
                    $("#form-result").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; Une erreur est survenue !</div>');
                });
            }
        });
        return false;
    }
    /* form submit */

});
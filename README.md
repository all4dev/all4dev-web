All 4 Dev - API
===================

Required
---------
Composer
https://getcomposer.org/download/
```
curl -sS https://getcomposer.org/installer | php
```

Install
---------
```
php composer.phar install
```